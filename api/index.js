import request from '@/common/request.js'
import {
	formatGetUri
} from '@/common/util.js'

const api = {}
const PORT1 = 'baseinfo'
let type=1
// GET 请求方式
api.register = params => request.globalRequest(`${PORT1}/mobile/signUp`, 'GET', params, 1) 
// 登录
api.login = params => request.globalRequest('/login.do','POST',params,1);
//获取选项卡列表
api.tabList = params => request.globalRequest('/tab','GET',params,1);
//获取新闻列表
api.newsList = params => request.globalRequest(`/news`,'GET',params,2);
//获取新闻内容
api.newsContent = params => request.globalRequest(`/news/${params}`,'GET',{},1);
//获取新闻评论
api.evaList = params => request.globalRequest(`/eva/${params}`,'GET',{},1);
//添加新闻评论
api.addEva = params => request.globalRequest('/eva','POST',params,1);
// GET请求方式 api.register = params => request.globalRequest(`${PORT1}/mobile/signUp${formatGetUri(params)}`, 'GET',{}, 1)
export default api
