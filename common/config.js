let url_config = ""

if(process.env.NODE_ENV === 'development'){
    // 开发环境
    // url_config = 'http://192.168.1.5:8033/b09c767d46693b6df662'
	url_config='http://127.0.0.1:8081'
}else{
    // 生产环境
    url_config = 'https://pro.com/'
}

export default url_config