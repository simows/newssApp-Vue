
// 使用本地缓存存储userToken
// 封装 检查登录状态、登录、登出逻辑
export default{
	
	check()  {
		try {
		    const token = uni.getStorageSync('userToken');
			if(token){
				console.log(token)
				return true;
			}
		} catch (e) {
			return false;
		    // error
		}
		return false;
		
	},
	
	login(user) {
		try {
		    uni.setStorageSync('userToken', user);
		} catch (e) {
		    // error
			console.log(e);
		}
	},
	logout() {
		try {
		    uni.removeStorageSync('userToken');
		} catch (e) {
		    // error
			console.log(e);
		}
	},
	getUser() {
		try {
		    const token = uni.getStorageSync('userToken');
			if(token){
				return token;
			}
		} catch (e) {
			return null;
		    // error
		}
		return null;
	}

	
}

