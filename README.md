# newssApp-Vue

#### 介绍
简单新闻/博客APP

#### 开发流程
##### uniapp

+ ~~快速了解文档~~

##### 数据处理部分

+ ~~将json数据请求，换成本地ajax~~
+ ~~封装axios~~
+ 考虑文章数据是否要使用vux（数据处理方式）

##### 业务逻辑部分

+ ~~完善根据分类拉取newslist~~
+ ~~完善根据newsid 加载文章内容~~
+ ~~完善根据newsid拉取对应文章评论逻辑~~
+ ~~添加新增评论逻辑~~
+ ~~添加个人中心组件~~
+ 个人中心逻辑实现
+ ~~文章内容使用json 在页面之间传递出错~~
+ 基于路由权限管理
+ 数据仓库

##### 视图显示说明

| 参数         | 取值    | 说明                             |      |
| ------------ | ------- | -------------------------------- | ---- |
| newsShowType | 1、2、3 | 1、2均只需要一张图片，3 对应三张 |      |


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
